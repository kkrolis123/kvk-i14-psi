import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-mvp-main',
  templateUrl: './mvp-main.component.html',
  styleUrls: ['./mvp-main.component.css']
})
export class MvpMainComponent implements OnInit {

  transactions = [];
  users = [];

  data = {
    money: null,
    moneyUsedFor: null,
    userId: null,
  };

  historyData = {
    money: null,
    moneyUsedFor: null,
    user: null
  }

  constructor(private http : HttpClient) {
    this.http.get('http://localhost:3000/history/')
    .toPromise().then(data => {
      for (let key in data)
        if (data.hasOwnProperty(key))
          this.transactions.push(data[key]);
    });

    this.http.get('http://localhost:3000/user/')
    .toPromise().then(data => {
      for (let key in data)
        if (data.hasOwnProperty(key))
          this.users.push(data[key]);
    });
   }

   onClickSubmitAdd(moneyamount: number) {
    const currentMoney: number = this.users[0].money;
    const updatedMoney: number = +currentMoney + +moneyamount;

    this.users[0].money = updatedMoney;
    this.data.money = updatedMoney;

    this.addHistory(moneyamount.toString(), 'dalykas', this.users[0].id)

     return new Promise((resolve, reject) => {
       this.http.put('http://localhost:3000/user/' + this.users[0].id, { ...this.data })
         .subscribe((response: any) => {
            resolve(response);
         });
    });
  }

  onClickSubmitRemove(moneyamount: string) {
    const currentMoney: number = this.users[0].money;
    const updatedMoney: number = +currentMoney - +moneyamount;

    this.users[0].money = updatedMoney;
    this.data.money = updatedMoney;

    this.addHistory("-" + moneyamount.toString(), 'dalykas', this.users[0].id)

     return new Promise((resolve, reject) => {
       this.http.put('http://localhost:3000/user/' + this.users[0].id, { ...this.data })
         .subscribe((response: any) => {
            resolve(response);
         });
    });
  }

  addHistory(money: string, moneyUsedFor: string, user: number){
    this.historyData.money = money;
    this.historyData.moneyUsedFor = moneyUsedFor;
    this.historyData.user = user;

    this.transactions.push(this.historyData);

    return new Promise((resolve, reject) => {
      this.http.post('http://localhost:3000/history/', { ...this.historyData })
        .subscribe((response: any) => {
          resolve(response);
        });
    });
  }

  ngOnInit(): void {
  }

}
