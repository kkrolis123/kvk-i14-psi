import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MvpMainComponent } from './mvp-main.component';

describe('MvpMainComponent', () => {
  let component: MvpMainComponent;
  let fixture: ComponentFixture<MvpMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MvpMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MvpMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
