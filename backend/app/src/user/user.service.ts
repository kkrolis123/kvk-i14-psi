import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { CreateUserDto } from './userdto/create-user.dto';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(User)
        private readonly userReposotory: Repository<User>,
    ) {}

    create(createUserDto: CreateUserDto): Promise<User> {

        const user = new User();
        user.name = createUserDto.name;
        user.money = createUserDto.money
        return this.userReposotory.save(user);
    }

    async findAll (): Promise<User[]> {
        return this.userReposotory.find();
    }

    findOne(id: string): Promise<User> {
        return this.userReposotory.findOne(id);
    }

    async remove(id: string): Promise<void>{
        await this.userReposotory.delete(id);
    }

    async update(id: string, name: string, money: number): Promise<void>{
        const updatedUser = await this.findOne(id);
        if(name){
            updatedUser.name = name;
        }
        if(money){
            updatedUser.money = money;
        }

        await this.userReposotory.save(updatedUser);
    }
}
