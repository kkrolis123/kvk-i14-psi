import { Controller, Post, Body, Get, Param, Delete, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './userdto/create-user.dto';
import { User } from './user.entity';

@Controller('user')
export class UserController {

    constructor(private readonly userSrevice: UserService){}

    @Post()
    create(@Body() createBodyDto: CreateUserDto): Promise<User> {
        return this.userSrevice.create(createBodyDto);
    }

    @Get()
    findAll(): Promise<User[]> {
        return this.userSrevice.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: string): Promise<User> {
        return this.userSrevice.findOne(id);
    }

    @Delete(':id')
    remuve(@Param('id') id: string): Promise<void> {
        return this.userSrevice.remove(id);
    }

    @Put(':id')
    async update(
        @Param('id') id: string,
        @Body('name') name: string,
        @Body('money') money: number
    ) {
        await this.userSrevice.update(id, name, money);
        return null;
    }
}
