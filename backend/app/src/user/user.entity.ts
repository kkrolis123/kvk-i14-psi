import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from 'typeorm';
import { History } from 'src/history/history.entity';

@Entity()
export class User {
   
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ length: 255})
    name: string;

    @Column()
    money: number;

    @OneToMany(type => History, history => history.user)
    history: History;
}