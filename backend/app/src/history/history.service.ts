import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History } from './history.entity'
import { CreateHistoryDto } from './historydto/create-history.dto'

@Injectable()
export class HistoryService {
    constructor(
        @InjectRepository(History)
        private readonly historyReposotory: Repository<History>,
    ) {}

    create(createHistoryDto: CreateHistoryDto): Promise<History> {
        const history = new History;

        history.money = createHistoryDto.money;
        history.moneyUsedFor = createHistoryDto.moneyUsedFor;
        history.user = createHistoryDto.user;

        return this.historyReposotory.save(history);
    }

    findall(): Promise<History[]> {
        return this.historyReposotory.find();
    }
}
