import { Controller, Get, Post, Body } from '@nestjs/common';
import { History } from './history.entity';
import { HistoryService } from './history.service';
import { CreateHistoryDto } from './historydto/create-history.dto';

@Controller('history')
export class HistoryController {

    constructor(private readonly historyService: HistoryService){}

    @Post()
    create(@Body() createHistoryDto: CreateHistoryDto): Promise<History> {
        return this.historyService.create(createHistoryDto);
    }

    @Get()
    findAll(): Promise<History[]> {
        return this.historyService.findall();
    }
}
