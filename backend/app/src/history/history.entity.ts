import { Entity, Column, PrimaryGeneratedColumn, OneToOne, JoinColumn, OneToMany, ManyToOne } from 'typeorm';
import { User } from '../user/user.entity'

@Entity()
export class History{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    money: string;

    @Column()
    moneyUsedFor: string;

    @ManyToOne(type => User, user => user.history)
    @JoinColumn({ name: "userId"})
    user: User[];
}