import { User } from "src/user/user.entity";

export class CreateHistoryDto{
    money: string;
    moneyUsedFor: string;
    user: User[];
}